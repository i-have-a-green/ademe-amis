<?php
/**
 * Template for bbpress forum
 *
 * @link https://codex.bbpress.org/themes/theme-compatibility/getting-started-in-modifying-the-main-bbpress-template/
 *
 */
?>

<?php get_header(); ?>


<?php
if (have_posts()) : 
	while ( have_posts() ) :
		the_post();

		// Forum title
		echo '<header class="top-padding-regular">';

			echo '<div class="wrapper bg-banner">';
				echo '<i class="h2-like wrapper-large left is-centered">'. get_the_title() .'</i>';
			echo '</div>';

		echo '</header>';

		// Forum content
		echo '<main id="forum-content" class="wrapper above-bg-banner bg-banner-security">';
			echo '<div class="wrapper-large btm-padding-regular is-centered">';
			the_content();
			echo '</div>';
		echo '</main>';

	endwhile; // End of the loop.
endif;?>
	
<?php get_footer(); ?>
