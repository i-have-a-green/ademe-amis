<?php
/*
Template Name: Espace membre
*/
?>

<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php 
$current_user = wp_get_current_user();

// Page title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner">';
		echo '<h1 class="wrapper-medium center is-centered ">';
		echo get_the_title();
		if(is_user_logged_in()):
			_e(' de ', 'ademe');
			echo $current_user->display_name;
		endif;
		echo'</h1>';
	echo '</div>';
echo '</header>';


// Content
echo '<main class="wrapper above-bg-banner btm-padding-regular">';

	//Si l'utilisateur n'est pas conecté : Formulaire
	if(!is_user_logged_in()):

		// Login Form
		echo '<div id="connectForm" class="wrapper-small is-centered">';
			//echo '<button class="button-brd" onclick="toggleConnectModal()" >Fermer X</button>';
			echo'<h2 class="h3-like '.ihag_ami_color_class('', 'color1').'">'; _e( 'Connexion', 'ademe' ); echo'</h2>';
			echo '<p>Cet espace est réservé aux membres de la communauté</p>';
			wp_login_form(
				array(
					'remember'       => false,
					'label_username' => __( 'Identifiant', 'ademe' ),
					'label_password' => __( 'Mot de passe', 'ademe' ),
					'remember'		 => true,
					'redirect'       => get_permalink(get_field("connexion_redirect", "options")),
				)
			);
			echo '<a href="'. wp_lostpassword_url( home_url() ).'" class="link-default is-centered center">'; _e("Mot de passe oublié ?", "ademe"); echo '</a>';
		echo '</div>';

	//Si l'utilisateur est conecté : Menu
	else:

		echo '<nav class="wrapper above-bg-banner is-centered btm-padding-regular '.ihag_ami_color_class('', 'color1').'" id="member-nav">';
			ihag_the_post_thumbnail('block-sub-menu');
			// echo ihag_menu('member_area');
			// echo bbp_get_user_profile_url( get_current_user_id() );
			
			echo wp_nav_menu(array(
				'container' => false,                           // Remove nav container
				'menu_class' => 'menu-member_area',       // Adding custom nav class
				'items_wrap' => '<ul id="%1$s" class="%2$s" ><li><a href="'.bbp_get_user_profile_url( get_current_user_id()).'">Mon profil</a></li>%3$s</ul>',
				'theme_location' => 'member_area',                 // Where it's located in the theme
				'depth' => 2,                                   // Limit the depth of the nav
				'fallback_cb' => false,                         // Fallback function (see below)
				'walker' => new Walker_Nav_Menu(),
				
			  ));



		echo '</nav>';
	
	endif;

echo '</main>';
?>

<!-- End of the loop -->
<?php endwhile; endif;?>

<?php get_footer(); ?>