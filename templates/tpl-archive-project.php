<?php
/*
Template Name: Projet
*/
?>

<?php get_header(); ?>

<?php 
// Page title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
		echo '<h1 class="wrapper-medium left-for-desktop is-centered">'. get_the_title() .'</h1>';
	echo '</div>';

echo '</header>';


// Archive Content
echo '<main id="archive-content" class="wrapper above-bg-banner btm-padding-regular">';

	// Load Filters
	get_template_part( 'template-parts/part','taxo' ); 

	// Listing container
	echo '<div id="archive-listing">';

		$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
		$type = "project";
		$args = array(
			'paged' => $num_page,
			'post_type'   => $type,
			'tax_query' => array(
				'relation' => 'AND',
			)
		);

		$my_taxonomies = get_object_taxonomies('project');
		foreach($my_taxonomies as $my_taxonomy){
			$taxonomy = get_taxonomy($my_taxonomy);
			if(isset($_GET[$taxonomy->name])){
				$args['tax_query'][] = array(
					'taxonomy' => $taxonomy->name,
					'field'    => 'slug',
					'terms'    => $_GET[$taxonomy->name],
				);
			}
		}
			
		query_posts($args);
		global $wp_query; 

		if (have_posts()) : 

			echo '<div class="listing-project">';

				while (have_posts()) : the_post(); 

				get_template_part('template-parts/archive', "project");

				endwhile;

			echo '</div>';

			ihag_page_navi();
			
		else : 

			get_template_part( 'template-parts/content', 'none' );
			
		endif;

	echo '</div>';
	
echo '</main>';
?>

<?php get_footer(); ?>