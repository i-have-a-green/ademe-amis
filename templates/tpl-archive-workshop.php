<?php
/*
Template Name: Atelier
*/
?>

<?php get_header(); ?>

<?php 
// Page title
echo '<header class="top-padding-regular">';

    echo '<div class="wrapper bg-banner bg-banner-page-title">';
        echo '<h1 class="wrapper-medium left-for-desktop is-centered">'. get_the_title() .'</h1>';
    echo '</div>';

echo '</header>';


// Archive Content
echo '<main class="wrapper above-bg-banner btm-padding-regular">';

    $posts = get_posts( array(
        'post_type'         => 'workshop',
        'posts_per_page' 	=> -1,
        'meta_key'			=> 'date-start',
        'orderby'			=> 'meta_value_num',
        'order'				=> 'DESC',
        // 'meta_query' => array(
        //     array(
        //         'key' => 'date-start',
        //         'value' => date('Ymd'),
        //         'type' => 'DATE',
        //         'compare' => '>='
        //     )
        //     ),
    ) );

    if( $posts ):

        echo '<div class="listing-workshop wrapper-large is-centered">';

        foreach( $posts as $post ):
                setup_postdata( $post );
                get_template_part('template-parts/archive', "workshop"); 
        endforeach;

        echo '</div>'; 

        ihag_page_navi();
        wp_reset_postdata();

    else :
        get_template_part( 'template-parts/content', 'none' );
    endif;

    
echo '</main>';
?>

<?php get_footer(); ?>