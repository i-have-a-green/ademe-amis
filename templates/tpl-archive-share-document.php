<?php
/*
Template Name: Partage de documents
*/

//Check if connect
if(!is_user_logged_in()){
    wp_redirect(home_url(), 302);
} 
?>

<?php get_header(); ?>

<?php
// Page title
echo '<header class="top-padding-regular">';
	echo '<div id="decorativeBanner" aria-hidden="true" class="bg-banner bg-banner-page-title"></div>';

	echo '<div class="wrapper is-centered btm-padding-regular" id="modalAnchor">';
		echo '<h1 class="left no-margin">'. get_the_title() .'</h1>';
		echo '<button class="button-brd" onclick="toggleSFormDocument()">'; _e('Ajouter un document', 'ademe'); echo '</button>';
	echo '</div>';

	?>

	<!-- Modale d'upload -->
	<div id="documentModale" class="is-hidden wrapper">

		<button id="closeForm" class="button-brd is-centered" onclick="toggleSFormDocument()"><?php _e('Fermer', 'ademe') ;?> X</button>
		
		<!-- Upload Form -->		
		<form class="form white-bg" name="shareDocument" id="shareDocument" action="#" method="POST" enctype="multipart/form-data">
			<input type="hidden" value="" name="honeyPotcontact">

			<label for="fileDocument"><?php esc_html_e('Document à partager', 'ademe')?> *</label>
			<input class="button-brd" type="file" id="fileDocument" name="fileDocument" accept="doc/*" required>

			<label for="titleDocument"><?php esc_html_e('Titre du document', 'ademe')?> *</label>
			<input type="text" name="titleDocument" id="titleDocument" placeholder="<?php esc_html_e('Document n°', 'ademe')?>" required value="">
		
			<label for="thumbnailDocument"><?php esc_html_e("Image d'illustration", "ademe")?> </label>
			<input class="button-brd" type="file" id="thumbnailDocument" name="thumbnailDocument" accept="image/*">
		
			<i class="body-like">* <?php esc_html_e('Champs obligatoires', 'ademe')?></i>
			
			<input class="button" type="submit" id="sendDocument" value="Ajouter le document">
		</form>

	</div>
	
	<?php

echo '</header>';
?>

<?php 
// Archive Content
echo '<main class="wrapper btm-padding-regular">';

	// Condition import Documents
	$num_page = (get_query_var("paged") ? get_query_var("paged") : 1);
	$args = array(
		'paged'				=> $num_page,
		'post_type'			=> 'document',
		'post_status'		=> 'publish',
	);

	global $wp_query; 
	query_posts($args);

		if (have_posts()) : 

			echo '<div class="listing-document wrapper-large is-centered">';

				while (have_posts()) : the_post(); 

				get_template_part('template-parts/archive', "share-document");

				endwhile;

			echo '</div>';

			ihag_page_navi();
			
		else : 

			get_template_part( 'template-parts/content', 'none' );
			
		endif;

echo '</main>';
?>

<?php get_footer(); ?>