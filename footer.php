
</div><!-- div #content -->

<!-- First site -->
<?php switch_to_blog(1); ?>

<footer id="footer">
	<div class="wrapper">
		<div id="footer_top" class="wrapper-large is-centered">

			<!-- Footer Part 1 : Logo + RS -->
			<a id="footer-logo" class="link-discrete" href="<?php echo get_home_url(); ?>" title="<?php esc_html_e( 'Lien vers la page d\'accueil', 'ademe') ?>">
				<?php 
				//Custom Logo1
				$logo1 = get_field('logo1', 'option');
				$size = '';
				if( $logo1 ) { 
					echo wp_get_attachment_image( $logo1, $size );
				}
				//Custom Logo2
				$logo2 = get_field('logo2', 'option');
				$size = '';
				if( $logo2) { 
					echo wp_get_attachment_image( $logo2, $size );
				}?>
			</a><!-- #footer-logo -->

			<!--  Footer Part 2 : Divers liens en footer -->
			<?php 
			$text_footer = get_field('content', 'option');
			if ($text_footer) { 
				echo '<div id="footer-content" class="entry-content no-useless-margin">'. $text_footer .'</div>';
			}?>

			<!-- Footer Part 3 : Réseaux sociaux -->
			<div id="footer-social">
								
				<?php 
				// Lien vers la FAQ
				$link_faq = get_field('link_faq', 'option');
				if( $link_faq ): 
					$link_faq_url = $link_faq['url'];
					$link_faq_title = $link_faq['title'];
					$link_faq_target = $link_faq['target'] ? $link['target'] : '_self';
					?>
					<a id="footer_faq" class="link-black" href="<?php echo esc_url( $link_faq_url ); ?>" target="<?php echo esc_attr( $link_faq_target ); ?>" title="<?php echo esc_html( $link_faq_title ); ?>">
						<?php echo esc_html( $link_faq_title ); ?>	
					</a>
				<?php endif; 

				// Lien vers email contact
				$link_contact = get_field('contact', 'option');
				if( $link_contact ): 
					$link_contact_url = $link_contact['url'];
					$link_contact_title = $link_contact['title'];
					$link_contact_target = $link_contact['target'] ? $link['target'] : '_self';
					?>
					<a id="footer_faq" class="link-black" href="<?php echo antispambot( $link_contact_url ); ?>" target="<?php echo esc_attr( $link_contact_target ); ?>" title="<?php echo esc_html( $link_contact_title ); ?>">
						<?php echo esc_html( $link_contact_title ); ?>	
					</a>
				<?php endif; ?>


				<?php 
				// Réseaux sociaux
				$facebook = get_field('facebook', 'option');
				$twitter = get_field('twitter', 'option');
				$linkedin = get_field('linkedin', 'option');
				$youtube = get_field('youtube', 'option');

				if( $facebook ): 
					$link = $facebook;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
						<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/facebook.svg" height="22" width="22">
					</a>
				<?php 
				endif; 

				if( $twitter ): 
					$link = $twitter;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
						<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.svg" height="22" width="22">
					</a>
				<?php 
				endif; 
			
				if( $linkedin ): 
					$link = $linkedin;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
						<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.svg" height="22" width="22">
					</a>
				<?php 
				endif;

				if( $youtube ): 
					$lik = $youtube;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
						<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/youtube.svg" height="22" width="22">
					</a>
				<?php endif; ?>
			</div>

		</div><!-- End of #footer_top -->
	</div><!-- End of .wrapper -->

	<div id="footer_bottom" class="gray-light-bg center">
		<div class="wrapper no-useless-margin">
			<?php ihag_menu('Last') ;?>
			<ul class="is-centred wrapper-large no-useless-margin">
				<li id="footer_name" ><p class="no-margin">Site web éco-conçu par <a href="https://www.agence-muscade.fr/">Muscade</a></p></li>
				<li id="footer_name2" ><p class="no-margin">Co-élaboré et animé par <a href="http://www.oree.org/">ORÉE</a></p></li>
				<li id="footer_copyright" ><p class="no-margin">© <?php echo date("Y"); ?> ADEME - Tous droits réservés</p></li>
			</ul>
		</div><!-- End of .wrapper -->
	</div><!-- End of #footer_bottom -->

</footer><!-- End of #footer -->

<?php restore_current_blog(); ?>

<?php wp_footer(); ?>

</body>
</html>
