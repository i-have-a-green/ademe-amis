<?php
/**
 * Template part for displaying page archive-workshop in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>
<article class="workshop-card">

	<!-- Date -->	
	<p class="event-date text-small uppercase no-margin">
		<?php  
		// Date
		echo '<time  datetime="'. date_i18n($format = 'Y-m-d', strtotime(get_field('date-start'))) .'">';
		echo date_i18n( get_option('date_format'), strtotime(get_field('date-start')));

		if(get_field('need_second_date') == true):
			if(!empty(get_field('second_date'))):
				_e(' au ', 'ademe');
				echo date_i18n( get_option('date_format'), strtotime(get_field('second_date')));
			endif;
		endif;
		echo '</time>';
		?>
	</p>

	<!-- Title -->
	<div class="event-title">
		<a class="link-color <?php echo ihag_ami_color_class('', 'color1'); ?>" href="<?php the_permalink();?>" title="">
			<?php the_title('<h2 class="h3-like no-margin">', "</h2>");?>
			<?php
			// Subtitle (option)
			$subtitle = get_field('sub_head');
			if(!empty($subtitle)):
				echo '<h3 class="h4-like no-margin '.ihag_ami_color_class('', 'color2').'">'. $subtitle.'</h3>';
			endif;
			?>
			
		</a>
		<!--<p class="no-margin">Subtitle</p>-->
	</div>

	<!-- Button -->
	<a class="button-brd" href="<?php the_permalink();?>" title="<?php _e('Lire l\'évènement ', 'ademe'); the_title();?>">
		<p class="no-margin"><?php _e('Lire', 'ademe'); ?></p>
	</a>

</article>
