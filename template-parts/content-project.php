<?php
/**
 * Template part for displaying page content-project in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */
?>


<?php 
// Project title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
		echo '<h1 class="h2-like wrapper-medium left-for-desktop is-centered '. ihag_ami_color_class('', 'color1') .'">'. get_the_title() .'</h1>';

		// Project Subtitle
		if(get_field('sub-head')):
			echo '<h2 class="h3-like wrapper-medium left-for-desktop is-centered '. ihag_ami_color_class('', 'color2') .' ">'. get_field('sub-head') .'</h2>';
		endif;
        
		// Project Thumbnail
		if ( has_post_thumbnail() ) {
			echo '<div class="wrapper-small center is-centered thumbnail-container top-padding-tiny">';
				the_post_thumbnail( 'wrapper-large-half', ['class' => 'img-responsive']);
				if (get_the_post_thumbnail_caption()) {
					echo '<i class="italic left">'. get_the_post_thumbnail_caption() .'</i>';
				}
			echo '</div>';
		}
		
	echo '</div>';
echo '</header>';

// Share
get_template_part( 'template-parts/part','share' );

// Project content
if ( get_the_content() ) {
	echo '<main id="raw-content" class="above-bg-banner bg-banner-security">';
	the_content();
	echo '</main>';
} else {
	get_template_part( 'template-parts/content', 'none' );
}


// Project pagination
$prev_post = get_previous_post();
$next_post = get_next_post();

if ( (!empty( $prev_post )) || (!empty( $next_post )) ):

	echo '<nav class="wrapper is-centered">';

		echo '<div id="project-nav"  class="wrapper-large is-centered '; if (empty( $prev_post )) { echo 'right';} echo '">';

			if (!empty( $prev_post )): 
				echo '<a class="left link-black v-padding-tiny" href="'.get_permalink( $prev_post->ID ).'">';
					echo '<img src="'. get_template_directory_uri().'/image/arrow-left.svg" alt="'; _e("Précédent", "ademe"); echo '" width="16" height="16">';
					echo '<i class="body-like uppercase h6-like no-margin">'; _e("Ressource précédente", "ademe"); echo '</i>';
					echo '<p class="no-margin">'. $prev_post->post_title .'</p>';
				echo '</a>';
			endif; 

			if (!empty( $next_post )): 
				echo '<a class="right link-black v-padding-tiny" href="'.get_permalink( $next_post->ID ).'">';
					echo '<img src="'. get_template_directory_uri().'/image/arrow-right.svg" alt="'; _e("Suivant", "ademe"); echo '" width="16" height="16">';
					echo '<i class="body-like uppercase h6-like no-margin">'; _e("Ressource suivante", "ademe"); echo '</i>';
					echo '<p class="no-margin">'. $next_post->post_title .'</p>';
				echo '</a>';
			endif; 

		echo '</DIV>';

	echo '</nav>';

endif; 
?>