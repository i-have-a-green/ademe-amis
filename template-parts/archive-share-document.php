<?php
/**
 * Template part for displaying page archive-partner in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article class="document-card">

	<!-- Thumbnail -->
	<a class="img-container link-discrete right" title="<?php the_title();?>">
		<?php ihag_the_post_thumbnail('archive-ressource', $attr = array( "class" => " img-responsive" ));?>
	</a>

	<div class="txt-container">

		<!-- Title -->
		<h2 class="h3-like no-margin <?php echo ihag_ami_color_class('', 'color1'); ?> "><?php the_title();?></h2>
		<?php the_content(); ?>

	</div>

	<!-- Document -->


</article>


