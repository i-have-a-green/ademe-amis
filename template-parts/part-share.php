<?php
/**
 * Template part for displaying shareable links
 */
?>

<aside id="share" class="wrapper center btm-padding-tiny above-bg-banner">
	<a class="JSrslink link-icon" href="https://www.twitter.com/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
		<span class="sr-only"><?php esc_html_e( 'Partager sur Twitter', 'ademe' ); ?></span> 
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/twitter.svg" height="24" width="24" aria-hidden="true">
	</a>
	<a class="JSrslink link-icon" href="https://www.linkedin.com/cws/share?url=<?php echo nbTinyURL(get_the_permalink());?>">
		<span class="sr-only"><?php esc_html_e( 'Partager sur Linkedin', 'ademe' ); ?></span> 
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/image/linkedin.svg" height="24" width="24" aria-hidden="true">
	</a>
</aside>