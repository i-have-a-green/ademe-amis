<?php
/**
 * Template part for displaying page content-workshop in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>


<?php 
// Workshop title
echo '<header class="top-padding-regular">';

	echo '<div class="wrapper bg-banner bg-banner-page-title">';
		echo '<h1 class="h2-like wrapper-medium left-for-desktop is-centered '. ihag_ami_color_class('', 'color1') .'">'. get_the_title() .'</h1>';

		// Workshop Subtitle
		if(get_field('sub_head')):
			echo '<h2 class="h3-like wrapper-medium left-for-desktop is-centered '. ihag_ami_color_class('', 'color2') .' ">'. get_field('sub_head') .'</h2>';
		endif;

		// Workshop Date
		echo '<time class="body-like wrapper-medium is-centered uppercase" datetime="'. date_i18n($format = 'Y-m-d', strtotime(get_field('date-start'))) .'">';
			echo date_i18n( get_option('date_format'), strtotime(get_field('date-start')));

            if(get_field('need_second_date') == true):
                if(!empty(get_field('second_date'))):
					_e(' au ', 'ademe');
                    echo date_i18n( get_option('date_format'), strtotime(get_field('second_date')));
                endif;
			endif;
			
        echo '</time>';
        
		// Workshop Thumbnail
		if ( has_post_thumbnail() ) {
			echo '<div class="wrapper-small center is-centered thumbnail-container top-padding-tiny">';
				the_post_thumbnail( 'wrapper-large-half', ['class' => 'img-responsive']);
				if (get_the_post_thumbnail_caption()) {
					echo '<i class="italic left">'. get_the_post_thumbnail_caption() .'</i>';
				}
			echo '</div>';
		}
		
	echo '</div>';
echo '</header>';

// Share
get_template_part( 'template-parts/part','share' );

// Event content
if ( get_the_content() ) {
	echo '<main id="raw-content" class="above-bg-banner bg-banner-security">';
	the_content();
	echo '</main>';
} else {
	get_template_part( 'template-parts/content', 'none' );
}
?>
