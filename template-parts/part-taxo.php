<?php
/**
 * Template part for displaying page part taxo.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>


<?php 
$my_taxonomies = get_field('taxonomies');
if ($my_taxonomies) : ?>

<aside id="archive-settings">

	<div id="archive-buttons">

		<button class="button-brd tablet-hidden desktop-hidden huge-hidden uppercase" onclick="toggleFilter()" aria-label="<?php esc_html_e( 'Afficher les filtres', 'ademe' ); ?>">
			<?php esc_html_e( 'Afficher les filtres', 'ademe' ); ?>
		</button>

		<button class="button-brd tablet-hidden desktop-hidden huge-hidden uppercase" onclick="toggleFilter()" aria-label="<?php esc_html_e( 'Masquer les filtres', 'ademe' ); ?>">
			<?php esc_html_e( 'Masquer les filtres', 'ademe' ); ?>
		</button>

	</div>

	<!-- Form -->
	<form action="<?php the_permalink();?>" method="get" id="archive-filter" name="archive-filter">

		<div id="filter-content">

			<h3 class="h6-like"><?php _e('Filtrer par :', 'ademe');?></h3>
			<!-- <input type="checkbox" name="all" value="" id="">
			<label for="all">Tous</label> -->

			<?php 
			foreach($my_taxonomies as $my_taxonomy):

				$taxonomy = get_taxonomy($my_taxonomy['taxonomy']);
				$terms = get_terms( 
					array(
						'taxonomy' => $taxonomy->name,
						'hide_empty' => false,
					) 
				);
				?>

					<details open class="btm-padding-small">

						<summary><?php echo $taxonomy->label;?></summary>
						<?php 
						foreach($terms as $term):
							$checked = '';
							if(isset($_GET[$taxonomy->name])){
								$gets = $_GET[$taxonomy->name];
								foreach($gets as $get){
									if($term->slug == $get){
										$checked = 'checked';
										break;
									}
								}
							}
							?>
							<?php
								$class = '';
								if($taxonomy->name == "ami"){
									//$class =  'ami'.get_field('ami', $term).'-color1';
									$ami_id = $term->term_id;
									$class = 'ami'.$ami_id.'-color1';
								}
							?>			
							<label for="<?php echo $term->slug;?>" class="custom-checkbox <?php echo $class ?>">
								<?php echo $term->name;?>
								<input type="checkbox" class="custom-checkbox-input" name="<?php echo $taxonomy->name;?>[]" value="<?php echo $term->slug;?>" id="<?php echo $term->slug;?>" <?php echo $checked; ?>>
								<span class="checkmark"></span>
							</label>

						<?php endforeach;?>

					</details>
					
				<?php
			endforeach;
			?>

		</div><!-- /#filter-content -->

		<!--<button type="submit"><?php //_e('Valider', 'ademe');?></button>-->
	</form>

</aside>

<?php
else : 

	// Message de debug
	echo "<!-- Aucune taxonomie trouvée -->";
	echo '<span id="archive-settings" aria-hidden="true"></span>';

endif;
?>