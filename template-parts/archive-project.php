<?php
/**
 * Template part for displaying page archive-project in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<article class="project-card">

	<?php 
	// Thumbnail
	$size='archive-project';
	echo '<a class="project-thumb link-discrete" href="'. get_permalink() .'" title="'. get_the_title().'">';
		echo ihag_the_post_thumbnail($size, $attr = array( "class" => "img-in-link img-responsive" ));
	echo '</a>';

	// Title
	echo '<a class="link-color" href="'. get_permalink() .'" title="'. get_the_title().'">';
		echo '<h2 class="h3-like no-margin '.ihag_ami_color_class('', 'color1').'  ">'. get_the_title().'</h2>';

		// Subtitle (option)
		$subtitle = get_field('sub-head');
		if(!empty($subtitle)):
			echo '<h3 class="h4-like no-margin '.ihag_ami_color_class('', 'color2').'">'. $subtitle.'</h3>';
		endif;
	echo '</a>';
	?>

</article>


