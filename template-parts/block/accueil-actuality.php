<?php
/**
 * Block Name: Bloc accueil-actualité
 */
 ?>

<section <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="blk-img v-padding-small">

<?php
$hook = get_field('hook');

if ( !$hook ):?>
	<em>Renseigner le contenu</em>
	
<?php else :?>

	<div class="wrapper">
	
		<?php 

		if(!empty(get_field('date'))):
			$date = get_field('date');
			_e($date, "ademe");
		endif;

		if(!empty(get_field('image'))):
			$image = get_field('image');
			$size = 'medium';
			echo wp_get_attachment_image($image, $size); 
		endif;

		if(!empty(get_field('legend'))):
			$legend = get_field('legend');
			echo(_e($legend, "ademe"));
		endif;

		?><br><?php
		// share on socials medias
			$link = get_field('twitter', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.svg" height="24" width="24">
				</a>
			<?php endif; 
			?>

			<?php 
			$link = get_field('linkedin', 'option');
			if( $link ): 
				$link_url = $link['url'];
				$link_title = $link['title'];
				$link_target = $link['target'] ? $link['target'] : '_self';
				?>
				<a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
					<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.svg" height="24" width="24">
				</a>
			<?php endif;
			// end share

		echo(_e($hook, "ademe"));
		
		?> 
	
	</div>

<?php endif; ?>
</section>
