<?php
/**
 * Block Name: Boite à outils
 */
 ?>

<section <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="wp-block blk-download wrapper btm-padding-regular">

<?php

$doc = get_field('documents');
if ( !$doc ):

	echo '<em>Renseigner le contenu</em>';
	
else :

	$title = get_field('title');

	if(!empty($title)):
		echo '<h2 class="left-for-desktop wrapper-medium is-centered">'. $title. '</h2>';
	endif;

	// Check rows exists, then loop through rows.
	if( have_rows('documents') ):
		echo '<ul class="wrapper-medium is-centered toolbox-wrapper">';
		while( have_rows('documents') ) : the_row();
			echo '<li class="">';
				$tool_title = get_sub_field('tool_title');
				if (!empty($tool_title)) {
					echo '<h3 class="h4-like">'. $tool_title .'</h3>';
				}
				
				$description = get_sub_field('description');
				if(!empty($description)):
					echo '<p class="">'. $description .'</p>';
				endif;

				$document = get_sub_field('download_document');
				if(!empty($document)):		
					echo '<a class="link-discrete" href="'. $document['url'] .'" title="'. $document['filename'] .'"  download>';
						echo '<img class="img-in-link" src="'. get_template_directory_uri(). '/image/download.svg" alt="'; _e('Télécharger', 'ademe'); echo '" width="38" height="38">';
						echo '<h4 class="body-like no-margin">'. $document['filename'] .'</h4>';
					echo '</a>';
				endif;
			echo '</li>';
		endwhile;
		echo '</ul>';
	endif;
endif; ?>

</section>
