<?php
/**
 * Block Name: Contact
 */
 ?>

<section <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="wp-block blk-contact wrapper v-padding-small center">

<?php
$title = get_field('title');
$email = get_field('email');
$contact = get_field('contact');

if ( empty($contact) ):

	echo '<em>Renseigner le bloc</em>';

else :

	echo '<div class="wrapper-medium is-centered border v-padding-small">';

		if ( !empty($title) ) {
			echo '<h2 class="h3-like">'. $title .'</h2>';
		}

		if (!empty($contact)) {
			echo '<div class="entry-content">'. $contact .'</div>';
		}

		if (!empty($email)) {
		?>
			 <a class="button" href="mailto:<?php echo antispambot( $email); ?>"><?php echo antispambot( $email); ?></a>
		<?php }

	echo '</div>';

endif; ?>

</section>

