<?php
/**
 * Block Name: Bloc Contexte AMI
 */
 ?>

<section <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="wp-block blk-contexte-ami btm-padding-regular bg-banner wrapper">

<?php

$content = get_field('content');
/* if ( !$content ):

    echo '<em>Renseigner le contenu</em>';

else : */

    $title = get_field('title');
    if($title) {
        echo '<h1 class="wrapper-medium is-centered left-for-desktop h2-like '.ihag_ami_color_class('', 'color1').'">'. $title .'</h1>';
    }

    echo '<div class="layout wrapper-large is-centered">';

        // Image
        $image = get_field('image');
        if(!empty($image)):
            $size = 'wrapper-large-half';
            echo '<div class="wrapper-large img-container">';
                echo wp_get_attachment_image($image, $size);
                echo '<i class="body-like">'. wp_get_attachment_caption( $image ) .'</i>';
            echo '</div>'; 
        endif;

        // Links
        if( have_rows('links') ):

            echo '<nav class="right lnk-container">';

            while( have_rows('links') ) : the_row();

                $link = get_sub_field('page');
                if( $link ): 
                    $link_url = $link['url'];
                    $link_title = $link['title'];
                    $link_target = $link['target'] ? $link['target'] : '_self';
                    ?>
                    <a class="button-brd icon-left" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
                        <img src="<?php echo get_template_directory_uri();?>/image/arrow-left.svg" alt="<?php _e('Précédent', 'ademe'); ?>" width="16" height="16">
                        <?php echo esc_html( $link_title );?>
                    </a>
                    <?php 
                endif;

            endwhile;

            echo '</nav>';

        endif;

        // Content text
        if(!empty($content)):
            echo '<div class="txt-container"><div class="entry-content">'. $content .'</div></div>';
        endif;

    echo '</div>'; // End of .layout
    
/* endif; */ 
?>

</section>
