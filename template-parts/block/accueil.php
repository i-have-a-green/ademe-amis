c<?php
/**
 * Block Name: Bloc accueil
 */
 ?>

 <?php /*

<section class="blk-img v-padding-small">

<?php
$content = get_field('content');

if ( !$content ):?>
	<em>Renseigner le contenu</em>
	
<?php else :?>

	<div class="wrapper">
	
		<?php 

		if(!empty(get_field('date'))):
			$date = strtoupper(get_field('date'));
			_e("$date", "ademe");
		endif;

		if(!empty(get_field('image'))):
			$image = get_field('image');
			$size = 'medium';
			echo wp_get_attachment_image($image, $size); 
		endif;

		if(!empty(get_field('content'))):
			_e("$content", "ademe");
		endif;
		
		?> 
	
	</div>

<?php endif; ?>
</section>

*/ ?>