<?php
/**
 * Block Name: Liens
 */
 ?>
<!-- Bloc lien -->
<nav <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="wp-block blk-download wrapper bg-banner bg-banner-h3-title btm-padding-regular">

<?php
$links = get_field('links');

if ( !$links ):

	echo '<em>Renseigner le contenu</em>';
	
else :

	// Title
	$title = get_field('title');
	if(!empty($title)):
		echo '<h3 class="h2-like left-for-desktop wrapper-medium '. ihag_ami_color_class('', 'color1') .' is-centered">'. $title. '</h3>';
	endif;


	// List of Links
	if( have_rows('links') ):

		echo '<ul class="wrapper-medium center is-centered">';

		while( have_rows('links') ) : the_row();

			// Single link
			echo '<li class="left display-basic">';
				
			$link = get_sub_field('link');
			if($link):

				$link_target = $link['target'] ? $link['target'] : '_self';
				$link_url = $link['url'];
				$link_title = $link['title'];
				// Fallback in case there is no $link_title
				if (empty($link_title)) {
					$link_title = $link_url;
				}

				echo '<a class="link-discrete" href="'. $link_url .'" title="'. $link_title .'" target="'. $link_target .'">';
					// Icon changes according to $link['target'] : same window / different window
					if ($link['target']) { 
						echo '<img class="img-in-link" src="'. get_template_directory_uri(). '/image/open-window.svg" alt="'; _e('Ouvrir le lien dans une fenêtre externe', 'ademe'); echo '" width="38" height="38">'; 
					} else {
						echo '<img class="img-in-link" src="'. get_template_directory_uri(). '/image/link.svg" alt="'; _e('Ouvrir le lien dans la une fenêtre actuelle', 'ademe'); echo '" width="38" height="38">'; 
					}
					echo '<h4 class="body-like no-margin">'. $link_title .'</h4>';
				echo '</a>';

			endif;

			echo '</li>';

		endwhile;

		echo '</ul>';

	endif;

endif; ?>
</nav>

<!-- Fin Bloc lien -->