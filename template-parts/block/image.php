<?php
/**
 * Block Name: Bloc Image
 */
 ?>

<div <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="wp-block blk-img btm-padding-regular center wrapper">

<?php
$image = get_field('image');

if ( !$image ):
	echo '<em>Renseigner l\'image</em>';
	
else :

	echo '<div class="wrapper-large center is-centered entry-content">';

		$size = 'fullscreen';
		echo ihag_get_attachment_image($image, $size, $attr = array( "class" => "img-responsive" )); 
		if(!empty(get_field('legend'))):
			echo '<i class="italic center">'. get_field('legend') .'</i>';;
		endif;
	
	echo '</div>';

endif; 
?>

</div>
