<?php
/**
 * Block Name: Document(s) à télécharger
 */
 ?>

<section <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="wp-block blk-download wrapper btm-padding-regular">

<?php
$doc = get_field('documents');
if ( !$doc ):

	echo '<em>Renseigner le contenu</em>';
	
else :

	$title = get_field('title');

	if(!empty($title)):
		echo '<h3 class="left-for-desktop wrapper-medium is-centered">'. $title. '</h3>';
	endif;

	// Check rows exists, then loop through rows.
	if( have_rows('documents') ):

		// If true, design is different
		$display_option = get_field('display_basic');

		if ($display_option) {
			echo '<ul class="wrapper-large center is-centered">';
		} else {
			echo '<ul class="wrapper-medium center is-centered">';
		}

		while( have_rows('documents') ) : the_row();


			if ($display_option) {
				echo '<li class="left display-image">';
			} else {
				echo '<li class="left display-basic">';
			}

					if ($display_option) {

						$image = get_sub_field('image');
						$size = 'block-donwload';
						if(!empty($image)):
							echo ihag_get_attachment_image( $image, $size, $attr = array( "class" => "doc-thumb" ) );
						endif;

					}

					$document = get_sub_field('download_document');
				
					if(!empty($document)):		
						echo '<a class="link-discrete" href="'. $document['url'] .'" title="'. $document['filename'] .'"  download>';
							echo '<img class="img-in-link" src="'. get_template_directory_uri(). '/image/download.svg" alt="'; _e('Télécharger', 'ademe'); echo '" width="38" height="38">';
							echo '<h4 class="body-like no-margin">'. $document['filename'] .'</h4>';
						echo '</a>';
					endif;

					if ($display_option) {
						$description = get_sub_field('description');
						if(!empty($description)):
							echo '<p class="no-margin">'. $description .'</p>';
						endif;
					}

			echo '</li>';

		endwhile;

		echo '</ul>';

	endif;

endif; ?>

</section>
