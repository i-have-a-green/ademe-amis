<?php
/**
 * Block Name: Sous-menu
 */
 ?>

<section <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="blk-sub-menu wp-block wrapper bg-banner btm-padding-regular">

<?php

$menu = get_field('menu');

if ( empty($menu) ):

	echo '<em>Renseigner le bloc</em>';

else :

	$title = get_field('title');

	if ($title) {
		echo '<h2 class="center wrapper-medium is-centered margin-b '.ihag_ami_color_class('', 'color1').'">'.$title.'</h2>';
	}

	// Check rows exists.
	if( have_rows('menu') ):

		echo '<nav class="wrapper-large is-centered center">';

		// Loop through rows.
		while( have_rows('menu') ) : the_row();
		
			$size = 'block-sub-menu';
			$image = get_sub_field('image');
			$link = get_sub_field('link');

			if ($link) {
				echo '<a class="blk-submenu-item link-color" href="'. $link.'">';
			} else {
				echo '<div class="blk-submenu-item">';
			}
			?>
				<span class="image-container">
					<?php ihag_get_attachment_image($image, $size, $attr = array( "class" => "img-in-link" ));?>
				</span>
				<p class="h3-like menu-color"><?php echo get_sub_field('title');?></p>

			<?php 
			if ($link) {
				echo '</a>';
			} else {
				echo '</div>';
			}
			?>
		<?php
		// End loop.
		endwhile;

		echo '</nav>';

	endif;
	

endif; ?>

</section>

