<?php
/**
 * Block Name: Bloc présentation projet
 */
 ?>

<section <?php if (!empty($block['anchor'])) {echo 'id="' . $block['anchor'] . '"';} ;?> class="blk-info-project wp-block wrapper btm-padding-small">

<?php
$title = get_field('title_project');
$structure = get_field('structure');
$responsible = get_field('responsible'); 
$partner = get_field('partner');

if ( !$title ):

	echo '<em>Renseigner le contenu</em>';
	
else :

	echo '<ul class="wrapper-medium is-centered no-useless-margin">';

		if(!empty($title)):
			echo '<li>';
				echo '<p class="info-title '. ihag_ami_color_class('', 'color3') .'">'; _e('Titre du projet', 'ademe'); echo '</p>';
				echo '<h2 class="body-like">'. $title .'</h2>';
			echo '</li>';
		endif; 

		if(!empty($structure)):
			echo '<li>';
				echo '<p class="info-title '. ihag_ami_color_class('', 'color3') .'">';  _e('Structure porteuse', 'ademe'); echo '</p>';
				echo '<h2 class="body-like">'. $structure .'</h2>';
			echo '</li>';
		endif; 

		if(!empty($responsible)):
			echo '<li>';
				echo '<p class="info-title '. ihag_ami_color_class('', 'color3') .'">';  _e('Responsable du projet', 'ademe'); echo '</p>';
				echo '<h2 class="body-like">'. $responsible .'</h2>';
			echo '</li>';
		endif; 

		if(!empty($partner)):
			echo '<li>';
				echo '<p class="info-title '. ihag_ami_color_class('', 'color3') .'">';  _e('Partenaire(s)', 'ademe'); echo '</p>';
				echo '<h2 class="body-like">'. $partner .'</h2>';
			echo '</li>';
		endif; 
		
	echo '</ul>';
		
endif; ?>

</section>
