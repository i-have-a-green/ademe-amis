<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 */

?>

<?php 
// Page title
if ( !is_front_page() ) {

	echo '<header class="top-padding-regular">';

		echo '<div class="wrapper bg-banner">';
			echo '<i class="h1-like wrapper-medium left-for-desktop is-centered">'. get_the_title() .'</i>';
		echo '</div>';

	echo '</header>';
}

// Page content
if ( get_the_content() ) {
	
	if ( is_front_page() ) {
		echo '<div aria-hidden="true" class="top-padding-regular"></div>';
	}

	echo '<main id="raw-content" class="above-bg-banner bg-banner-security">';
	the_content();
	echo '</main>';
} else {
	get_template_part( 'template-parts/content', 'none' );
}
?>
