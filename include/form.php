<?php
add_action( 'init', 'contact_custom_post_type');
function contact_custom_post_type() {
	register_post_type( 'ihag_contact', /* (http://codex.wordpress.org/Function_Reference/register_post_type) */
		array('labels' 			=> array(
				'name' 				=> __('Contact', 'ihag'), /* This is the Title of the Group */
				'singular_name' 	=> __('Contact', 'ihag'), /* This is the individual type */
			), /* end of arrays */
			'menu_position' 	=> 18, /* this is what order you want it to appear in on the left hand side menu */
			'menu_icon' 		=> 'dashicons-email-alt', /* the icon for the custom post type menu. uses built-in dashicons (CSS class name) */
			'hierarchical' 		=> false,
			'public'             => false,
			'publicly_queryable' => false,
			'show_ui'            => true,
			'show_in_menu'       => false,
			'query_var'          => false,
			'supports' 			=> array( 'title', 'editor')
	 	) /* end of options */
	);
}

/*
* traitement du post du form de Contact
* enregistrement des values dans le custom post type
*/
add_action('rest_api_init', function() {
	register_rest_route( 'ihag', 'contactForm',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihag_contactForm',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	register_rest_route( 'ihag', 'shareDocument',
		array(
			'methods' 				=> 'POST', //WP_REST_Server::READABLE,
			'callback'        		=> 'ihag_shareDocument',
			'permission_callback' 	=> array(),
			'args' 					=> array(),
		)
	);

	
});


// function ihagFormNewsletter(WP_REST_Request $request){
	
// 	if (empty($_POST['honeyPot'])) {
// 		if(addUserMailChimp (sanitize_email( $_POST['newletter_email'] ))){
// 			echo "Votre adresse a bien été enregistrée.";
// 		}
// 		else{
// 			echo "Votre adresse existe dans notre base de données.";
// 		}
// 	}		
	
// 	return new WP_REST_Response( '', 200 );
// }

// function addUserMailChimp($email){
// 	// API to mailchimp ########################################################
// 	$list_id = get_field("list_id", 'option');
// 	$authToken = get_field("authToken", 'option');
// 	// The data to send to the API

// 	$postData = array(
// 		"email_address" => $email, 
// 		"status" => "subscribed", 
// 		/*"merge_fields" => array(
// 		"FNAME"=> $_POST["name"],
// 		"PHONE"=> $_POST["phone"])*/
// 	);

// 	// Setup cURL
// 	$ch = curl_init('https://us5.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
// 	curl_setopt_array($ch, array(
// 		CURLOPT_POST => TRUE,
// 		CURLOPT_RETURNTRANSFER => TRUE,
// 		CURLOPT_HTTPHEADER => array(
// 			'Authorization: apikey '.$authToken,
// 			'Content-Type: application/json'
// 		),
// 		CURLOPT_POSTFIELDS => json_encode($postData)
// 	));
// 	// Send the request
// 	//	$response = curl_exec($ch);
// 	$result = curl_exec($ch);
//     $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//     curl_close($ch);
// 	return ($httpCode == 200);
// }



function ihag_shareDocument(WP_REST_Request $request){
	if (empty($_POST['honeyPotcontact'])) {

		// Importer les fonctions d’admin
		if ( ! function_exists( 'media_handle_upload' ) ) {
			require_once( ABSPATH . 'wp-admin/includes/image.php' );
			require_once( ABSPATH . 'wp-admin/includes/file.php' );
			require_once( ABSPATH . 'wp-admin/includes/media.php' );
		}
		global $wpdb;
		
		if(isset($_FILES['fileDocument'])){
			$upload_dir   = wp_upload_dir();
			$uploaddirimg = $upload_dir['basedir'].'/fileDocument/';
			@mkdir($uploaddirimg, 0755);
			$uploadfile = $uploaddirimg . '-'.basename($_FILES['fileDocument']['name']);
			if (move_uploaded_file($_FILES['fileDocument']['tmp_name'], $uploadfile)) {
				$content = '<a class="link-discrete doc-content" href="'.$upload_dir['baseurl'].'/fileDocument/-'.basename($_FILES['fileDocument']['name']).'" download><img class="img-in-link" src="'. get_template_directory_uri(). '/image/download.svg" alt="Télécharger" width="38" height="38">'.basename($_FILES['fileDocument']['name']).'</a>';
			}
		}

		$post['post_type']   = 'document';
		$post['post_status'] = 'publish';
		$post['post_title'] = sanitize_text_field($_POST['titleDocument']); 
		$post['post_content'] = $content;
		$post_id = wp_insert_post( $post, true );
		
		
		if(isset($_FILES['thumbnailDocument'])){
			$uploadedfile = $_FILES['thumbnailDocument'];
			$upload_overrides = array( 
			'test_form' => false,
			);
			if(!empty($uploadedfile['name'])) {
				$movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
				if ( $movefile ) {
					$wp_filetype = $movefile['type'];
					$filename = $movefile['file'];
					$wp_upload_dir = wp_upload_dir();
					$attachment = array(
					'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
					'post_mime_type' => $wp_filetype,
					'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
					'post_content' => '',
					'post_status' => 'inherit'
					);
					$attach_id = wp_insert_attachment( $attachment, $filename, $post_id);
					$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
					wp_update_attachment_metadata( $attach_id, $attach_data );
					set_post_thumbnail( $post_id, $attach_id );
				}
			}
		}	
	

		

	}
	return new WP_REST_Response( '', 200 );
}


// wp_dashboard_setup is the action hook
add_action('wp_dashboard_setup', 'ihag_dashboard_contact');
function ihag_dashboard_contact() {
    wp_add_dashboard_widget('ihag_dashboard_contact', 'Export CSV','ihag_dashboard_contact_export');
}
function ihag_dashboard_contact_export(){
	?>
	<ul>
        <?php
        $args = array( 'posts_per_page' => 5, 'post_type' => 'ihag_contact' );
        $myposts = get_posts( $args );
        foreach ( $myposts as $post ) :
            echo '<li><a href="'.admin_url( 'post.php?post='.$post->ID.'&action=edit').'">'.get_the_date('',$post).' - '.$post->post_title.'</a></li>';
        endforeach;
        ?>
    </ul>
    <p>
    <a href="?report=ihag_export" class="button">Export</a>
    </p>
	<?php
}
class ihag_CSVExport
{
	/**
	* Constructor
	*/
	public function __construct()
    {
        if(isset($_GET['report']) && $_GET['report'] == 'ihag_export')
        {
        	$this->ihag_export();
        }
    }
	public function ihag_export(){
		global $wpdb, $post;
        $csv_fields=array();
        $csv_fields[] = 'Title';
		$csv_fields[] = 'Content';
		
        $output_filename = "contact_".date("Y-m-d H:i:s").'.csv';
        $output_handle = @fopen( 'php://output', 'w' );
        header( 'Cache-Control: must-revalidate, post-check=0, pre-check=0' );
        header( 'Content-Description: File Transfer' );
        header( 'Content-type: text/csv' );
        header( 'Content-Disposition: attachment; filename=' . $output_filename );
        header( 'Expires: 0' );
        header( 'Pragma: public' );
        // Insert header row
        fputcsv( $output_handle, $csv_fields,";" );
		$args = array( 'posts_per_page' => -1, 'post_type' => 'ihag_contact' );
		$myposts = get_posts( $args );
		foreach ( $myposts as $post ) : setup_postdata( $post );
			$tab_data = array( 
				get_the_title(), 
				str_replace(array(chr(10), chr(13)),' - ',$post->post_content), 
				// get_post_meta( $post->ID, 'firstname',true),
				// get_post_meta( $post->ID, 'name',true),
				// get_post_meta( $post->ID, 'email',true),
				// get_post_meta( $post->ID, 'comment',true)
			);
			fputcsv( $output_handle, $tab_data,";");
		endforeach;
        fclose( $output_handle );
		exit();
	}
}
// Instantiate a singleton of this plugin
new ihag_CSVExport();
