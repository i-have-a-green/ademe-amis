<?php
// Numeric Page Navi (built into the theme by default)
function ihag_page_navi($before = '', $after = '') {
	global $wpdb, $wp_query;
	$request = $wp_query->request;
	$posts_per_page = intval(get_query_var('posts_per_page'));
	$paged = intval(get_query_var('paged'));
	$numposts = $wp_query->found_posts;
	$max_page = $wp_query->max_num_pages;
	if ( $numposts <= $posts_per_page ) { return; }
	if(empty($paged) || $paged == 0) {
		$paged = 1;
	}
	$pages_to_show = 7;
	$pages_to_show_minus_1 = $pages_to_show-1;
	$half_page_start = floor($pages_to_show_minus_1/2);
	$half_page_end = ceil($pages_to_show_minus_1/2);
	$start_page = $paged - $half_page_start;
	if($start_page <= 0) {
		$start_page = 1;
	}
	$end_page = $paged + $half_page_end;
	if(($end_page - $start_page) != $pages_to_show_minus_1) {
		$end_page = $start_page + $pages_to_show_minus_1;
	}
	if($end_page > $max_page) {
		$start_page = $max_page - $pages_to_show_minus_1;
		$end_page = $max_page;
	}
	if($start_page <= 0) {
		$start_page = 1;
	}
	echo $before.'<nav class="post-navigation is-centered center">';
		echo '<ul>';
		if ($start_page >= 2 && $pages_to_show < $max_page) {
			$first_page_text = __( 'Premier', 'ademe' );
			echo '<li><a class="first" href="'.get_pagenum_link().'" title="'.$first_page_text.'">'.$first_page_text.'</a></li>';
		}

		if ( get_previous_posts_link()) {
			echo '<li class="previous">';
			echo '<img src="'. get_template_directory_uri().'/image/arrow-left.svg" alt="'; _e("Précédent", "ademe"); echo '" width="16" height="16">';
			previous_posts_link( __('Précédent', 'ademe'), 0 );
			echo '</li>';
		}

		for($i = $start_page; $i  <= $end_page; $i++) {
			if($i == $paged) {
				echo '<li class="current-cat"> '.$i.' </li>';
			} else {
				echo '<li><a href="'.get_pagenum_link($i).'">'.$i.'</a></li>';
			}
		}

		if ( get_next_posts_link()) {
			echo '<li class="next">';
			next_posts_link( __('Suivant', 'jointswp'), 0 );
			echo '<img src="'. get_template_directory_uri().'/image/arrow-right.svg" alt="'; _e("Suivant", "ademe"); echo '" width="16" height="16">';
			echo '</li>';
		}

		if ($end_page < $max_page) {
			$last_page_text = __( 'Dernier', 'jointswp' );
			echo '<li class="last"><a href="'.get_pagenum_link($max_page).'" title="'.$last_page_text.'">'.$last_page_text.'</a></li>';
		}
		echo '</ul>';
	echo '</nav>'.$after."";
} /* End page navi */