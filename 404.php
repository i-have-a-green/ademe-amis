<?php 
global $post;
switch_to_blog(1);
$page404 = get_field('page_404', 'option');
restore_current_blog();

if( $page404 ):

    switch_to_blog(1);
    
        $post = get_post($page404); setup_postdata($post);
        
        get_header();

        get_template_part( 'template-parts/content', get_post_type() );

        get_footer();

    restore_current_blog();
    
else:

    wp_redirect(home_url(), 301);
    

endif;


?>
