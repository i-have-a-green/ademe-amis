# Ademe : Expérimentations Urbaines

Prérequis :

- Node.js (https://nodejs.org/)
- npm (https://www.npmjs.com/)
- Wordpress (http://wordpress.org/latest.tar.gz)

1 - Installer Wordpress

2 - Installer le(s) pluggin(s) suivant(s) :
```console
ACF PRO
Custom Post Type UI
```

3 - Ajouter le thème Ademe à l'emplacement suivant :
```console
[projet] / wp-content / themes / ademe-amis
```

4 - Installer les dépendances avec npm
```console
$ npm install
```

5 - Lancer le script
```console
$ npm run watch
```

6 - Lancer BrowserSync [optionnel]
```console
$ npm run start
```

-----------

# Notes complémentaires :

Retirer les thèmes Wordpress inutiles (twentytwenty, twentynineteen, ect …)
```console
$ cd wp-content/themes/
$ rm -rf twenty
```

Modifier le chemin des scripts en fonction de l'environnement local
Chemin par défaut : 
```console
'https://ademe.local/'
package.json [ligne 44]
```


Pluggins Wordpress utilisés :
- ACF PRO - b3JkZXJfaWQ9OTE1Mzh8dHlwZT1kZXZlbG9wZXJ8ZGF0ZT0yMDE2LTEwLTEyIDE0OjMyOjI3
- Custom Post Type UI
- Yoast SEO
- WP Rocket