if (document.forms.namedItem("shareDocument")) {
    document.forms.namedItem("shareDocument").addEventListener('submit', function(e) {
        document.getElementById('sendDocument').disabled = true; //limit le problème des doubles clic
        e.preventDefault();

        var form = document.forms.namedItem("shareDocument");
        var formData = new FormData(form); //fonction native qui mets tout dans un format prêt a etre envoyé
        console.log(formData);
        xhr = new XMLHttpRequest(); //prepare la requete
        xhr.open('POST', resturl + 'shareDocument', true);
        xhr.onload = function() {
            if (xhr.status === 200) {
                //console.log(xhr.response);
                location.reload();
            }
        };
        xhr.send(formData);
    });
}