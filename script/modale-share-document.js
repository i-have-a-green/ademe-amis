function toggleSFormDocument() {
    document.getElementById("documentModale").classList.toggle("is-hidden");
}

document.addEventListener('DOMContentLoaded', function() {
    var els = document.getElementsByClassName("btn-modale-document");
    Array.prototype.forEach.call(els, function(el) {
        el.addEventListener("click", function(e) {
            e.preventDefault();
            e.stopPropagation();

            var modale = document.createElement("div");
            modale.classList.add("modale");
            modale.id = el.dataset.uniqId;

            var button = document.createElement("button");
            button.classList.add("closeModale");
            button.onclick = function() { document.getElementById(el.dataset.uniqId).remove(); };
            button.appendChild(document.createTextNode("X"));
            modale.appendChild(button);

            
            var modaleContent = document.createElement("div");
            modaleContent.id = 'documentModale' + el.dataset.uniqId;
            modaleContent.classList.add("documentModale");
            modale.appendChild(modaleContent);

            document.body.appendChild(modale);

        });
    });
});