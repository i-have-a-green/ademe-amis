/*
File structure :
------------------
1 - Open & Close menu
------------------
2 - Scroll detection
------------------
*/

// 1 - Open & Close menu (for mobile devices)
function toggleMenu() {
    document.getElementById("menu").classList.toggle("menu-open");
}

// 2 - Scroll detection : #topbar gets sticky on scroll
var menu = document.getElementById("topbar");
var scroll = menu.offsetHeight / 2;

window.onscroll = function() {
    if (window.pageYOffset > scroll) {
        menu.classList.add("scroll");
    } else {
        menu.classList.remove("scroll");
    }
};