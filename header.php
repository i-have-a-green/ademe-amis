<?php
/**
 * The header for our theme
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */

if(isset($_GET["deconnectUser"])){
	wp_logout();
	wp_redirect(home_url(), 302);
}

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-32px.png" sizes="32x32">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" sizes="124x124">
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" sizes="512x512">
	<link rel="icon" type="image/svg" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon.svg">
	<link rel="apple-touch-icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/favicon/favicon-124px.png" />
	<link rel="manifest" href="<?php echo get_template_directory_uri(); ?>/favicon/manifest.json">
	<meta name="theme-color" content="#ffffff">
	<?php wp_head(); ?>

	<style>

	<?php
		// Custom colors settings	

		// Color 1
		if (get_field('color1', 'option')) {
			echo '.color1  { color : '. get_field('color1', 'option').'; } ';
			echo '.bg-color1 { background : '. get_field('color1', 'option').'; } ';
		} else {
			echo '.color1 { color : #10A4B5; } ';
			echo '.bg-color1 { background : #10A4B5; } ';
		}

		// Color 2 + Titles H2
		if (get_field('color2', 'option')) {
			echo '.color2 { color : '.get_field('color2', 'option').'; } ';
			echo 'h2, .h2-like { color : '.get_field('color2', 'option').'; } ';
		} else {
			echo '.color2 { color : #148E83; } ';
			echo 'h2, .h2-like { color : #148E83; } ';
		}

		// Color 3 + H3 + Bold
		if (get_field('color3', 'option')) {
			echo '.color3 { color : '.get_field('color3', 'option').'; } ';
			echo 'h3, .h3-like, #content strong { color : '.get_field('color3', 'option').'; } ';
		} else {
			echo '.color3 { color : #06796E; } ';
			echo 'h3, .h3-like, #content strong { color : #06796E; } ';
		}

		// Color 4 + H4
		if (get_field('color4', 'option')) {
			echo '.color4 { color : '.get_field('color4', 'option').'; } ';
			echo 'h4, .h4-like { color : '.get_field('color4', 'option').'; } ';
		} else {
			echo '.color4 { color : #03645A; } ';
			echo 'h4, .h4-like { color : #03645A; } ';
		}

		// Menu Background
		if (get_field('color-menu', 'option')) {
			echo '.menu-primary_ami li { background-color : '.get_field('color-menu', 'option').'; } ';
			echo '.menu-color { color : '.get_field('color-menu', 'option').'; } ';
		} else {
			echo '.menu-primary_ami li { background-color : #000000; } ';
			echo '.menu-color { color : #000000; } ';
		}
	
	?>
	
	</style>
	<script type="text/javascript" src="https://tarteaucitron.io/load.js?domain=experimentationsurbaines.ademe.fr&uuid=2df1000bf385655312abede8e30421664e7d594e"></script>
</head>


<body <?php body_class(); ?>>

	<!-- Skip Links-->
	<a class="skip-link" tabindex="0"  href="#content"><?php esc_html_e( 'Accéder au contenu', 'ademe' ); ?></a>
	<a class="skip-link" tabindex="0" href="#menu"><?php esc_html_e( 'Menu', 'ademe' ); ?></a>
	<a class="skip-link" tabindex="0"  href="#footer"><?php esc_html_e( 'Accéder au pied de page', 'ademe' ); ?></a>

	<!-- If Logged in : show #login_bar  -->
	<?php 
	if(is_user_logged_in()):
		echo '<div id="login_bar" class="wrapper">';
			$user = wp_get_current_user() ; 
			
			echo '<p class="no-margin">'; 
				_e( 'Bonjour : ', 'ademe' );
				echo $user->display_name ;
			echo '</p>';

			echo '<a href="'.wp_logout_url( home_url() ).'" id="login_bar_button">';			
				_e('Se déconnecter', 'ademe');
			echo '</a>';
		echo '</div>';
	endif; 
	?>

	<!-- First site -->
	<?php switch_to_blog(1); ?>
	
	<nav id="topbar" class="h-padding-regular">
	
		<!-- Logo -->
		<a id="topbar-logo" class="link-discrete" href="<?php echo get_home_url(); ?>" title="<?php esc_html_e( 'Lien vers la page d\'accueil', 'ademe') ?>">
		<?php 
			//Custom Logos 
			$logo1 = get_field('logo1_header', 'options');
			$logo2 = get_field('logo2_header', 'options');
			$logoExpeUrba = get_field('logoExpeUrba', 'options');
			if( $logo1 ) { 
				echo wp_get_attachment_image( $logo1, "", false,);
			}
			if( $logo2) { 
				echo wp_get_attachment_image( $logo2, "", false,); 
			}
			?>
			<p><?php echo get_bloginfo('name'); ?></p>
			<?php
			if( $logoExpeUrba) { 
				echo wp_get_attachment_image( $logoExpeUrba, "", false,); 
			}
			?>
		</a>

		<!-- Burger button -->
		<button id="burger-menu" class="link-icon has-border " onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Ouvrir le menu', 'ademe' ); ?>">
			<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/burger.svg" height="18" width="24">
		</button>

		<!-- Menu -->
		<div id="menu">

			<!-- Close Button -->
			<button id="close-menu" class="link-icon has-border icon-left " onclick="toggleMenu()" aria-label="<?php esc_html_e( 'Fermer le menu', 'ademe' ); ?>">
				<img aria-hidden="true" src="<?php echo get_stylesheet_directory_uri(); ?>/image/cross.svg" height="20" width="30">
				<?php _e('Fermer', 'ademe');?>
			</button>

			<!-- Search -->		
			<form id="form-search" action="<?php echo home_url();?>" method="get" class="white-menu">
				<label for="search"><?php _e('Rechercher', 'ademe');?></label>
				<input type="text" name="s" id="search" placeholder="<?php _e("Tapez votre recherche ici", "digitemis");?>" value="<?php the_search_query(); ?>"/>
				<input type="image" class="link-icon has-border" id="search-image" alt="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" src="<?php echo get_template_directory_uri(); ?>/image/search.svg"  height="40" width="40">
			</form>

			<!-- Réseaux sociaux -->
			<ul id="header-social">
				<?php 
				$facebook = get_field('facebook', 'option');
				$twitter = get_field('twitter', 'option');
				$linkedin = get_field('linkedin', 'option');
				$youtube = get_field('youtube', 'option');

				if( $facebook ): 
					$link = $facebook;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/facebook.svg" height="22" width="22">
						</a>
					</li>
				<?php 
				endif;

				if( $twitter ): 
					$link = $twitter;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/twitter.svg" height="22" width="22">
						</a>
					</li>
				<?php 
				endif; 
				
				if( $linkedin ): 
					$link = $linkedin;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/linkedin.svg" height="22" width="22">
						</a>
					</li>
				<?php 
				endif;

				if( $youtube ): 
					$link = $youtube;
					$link_url = $link['url'];
					$link_title = $link['title'];
					$link_target = $link['target'] ? $link['target'] : '_self';
					?>
					<li>
						<a class="link-icon" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>" title="<?php echo esc_html( $link_title ); ?>">
							<img aria-hidden="true" src="<?php echo get_template_directory_uri(); ?>/image/youtube.svg" height="22" width="22">
						</a>
					<li>
				<?php endif; ?>
			</ul>

			<!-- Links -->
			<?php echo ihag_menu('primary'); ?>

		</div>

	</nav>

	<?php restore_current_blog(); ?>




	<?php 
	// menu du site AMI
	if(get_current_blog_id() != 1) :

		echo '<nav id="topbar-ami" class="center">';

		$img_bckg = get_field('background', 'options');
		$size='topbar-ami';

		if( $img_bckg ) {
			echo '<div id="topbar-background" class="bg-pattern" aria-hidden="true"></div>';
			echo wp_get_attachment_image( $img_bckg, $size, '', array( "id" => "topbar-img" ) );
		} else {
			echo '<div id="topbar-background" class="bg-pattern gray-light-bg" aria-hidden="true"></div>';
		}
	
			// Name site AMI
			echo '<a id="ami-title" class="link-discrete h1-like no-margin" href="'. home_url().'">'. get_bloginfo('name') .'</a>';
			echo ihag_menu('primary_ami');

		echo '</nav>';
	 
	endif;
	?>
	
	<!-- #content -->
	<div id="content">
